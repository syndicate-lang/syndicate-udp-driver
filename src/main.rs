use std::net::SocketAddr;
use std::sync::Arc;

use preserves_schema::Codec;
use syndicate::{
    actor::{Account, Activation, ActorResult, AnyValue, Cap, FacetRef, LinkedTaskTermination},
    dataspace::Dataspace,
    relay,
    schemas::trace,
    value::NestedValue,
};

mod schemas {
    include!(concat!(env!("OUT_DIR"), "/src/schemas/mod.rs"));
}

preserves_schema::define_language!(language(): Language<AnyValue> {
    syndicate: syndicate::schemas::Language,
    main: crate::schemas::Language,
});

use crate::schemas::udp;

use syndicate::enclose;
use syndicate_macros::during;
use syndicate_macros::on_message;

async fn run_udp_socket(
    b: &udp::Bind<AnyValue>,
    account: Arc<Account>,
    cause: Option<trace::TurnCause>,
    facet: FacetRef,
) -> ActorResult {
    let space = Arc::clone(&b.space);
    let sock = socket2::Socket::new(
        socket2::Domain::IPV4,
        socket2::Type::DGRAM,
        Some(socket2::Protocol::UDP))?;
    sock.set_reuse_address(true)?;
    sock.set_reuse_port(true)?;
    sock.set_nonblocking(true)?;
    sock.bind(&str::parse::<SocketAddr>(&format!("{}:{}", b.addr, b.port))?.into())?;
    let sock = Arc::new(tokio::net::UdpSocket::from_std(sock.into())?);
    facet.activate(&account, cause.clone(), enclose!((sock, space) move |t| {
        let local_addr = sock.local_addr()?;
        space.assert(t, language(), &udp::Bound {
            addr: local_addr.ip().to_string(),
            port: local_addr.port().into(),
        });
        enclose!(
            (sock) on_message!(t, space, language(), $p: udp::PacketOut, enclose!(
                (sock) move |_t: &mut Activation| {
                    tracing::trace!(?p, "outbound packet");
                    let target: SocketAddr =
                        str::parse(&format!("{}:{}", p.target_addr, p.target_port))?;
                    match sock.try_send_to(&p.body[..], target) {
                        Ok(_) => (),
                        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => (),
                        Err(e) => Err(e)?,
                    }
                    Ok(())
                })));
        enclose!(
            (sock, space) during!(t, space, language(), $g: udp::MulticastGroupMember, enclose!(
                (sock, space) move |t: &mut Activation| {
                    tracing::debug!(?g, "adding multicast group");
                    let group_address = str::parse(&g.group_address)?;
                    let group_interface = str::parse(&g.group_interface)?;
                    let mut ok = true;
                    if let Err(err) = sock.join_multicast_v4(group_address, group_interface) {
                        ok = false;
                        space.assert(t, language(), &udp::Error {
                            detail: AnyValue::new(format!("join_multicast_v4: {}", err)),
                        });
                    }
                    t.on_stop(enclose!((g) move |_t| {
                        if ok {
                            tracing::debug!(?g, "removing multicast group");
                            sock.leave_multicast_v4(group_address, group_interface)?;
                        }
                        Ok(())
                    }));
                    Ok(())
                })));
        enclose!(
            (sock, space) on_message!(t, space, language(), $l: udp::MulticastLoopback, enclose!(
                (sock, space) move |t: &mut Activation| {
                    tracing::debug!(?l, "setting multicast loopback");
                    if let Err(err) = sock.set_multicast_loop_v4(l.enabled) {
                        space.assert(t, language(), &udp::Error {
                            detail: AnyValue::new(format!("set_multicast_loop_v4: {}", err)),
                        });
                    }
                    Ok(())
                })));
        enclose!(
            (sock, space) during!(t, space, language(), $n: udp::MulticastTtl, enclose!(
                (sock, space) move |t: &mut Activation| {
                    tracing::debug!(?n, "setting multicast ttl");
                    let hop_count = match u32::try_from(&n.hop_count) {
                        Ok(hop_count) => hop_count,
                        Err(_) => {
                            space.assert(t, language(), &udp::Error {
                                detail: AnyValue::new("set_multicast_ttl_v4: bad hop count"),
                            });
                            return Ok(());
                        }
                    };
                    if let Err(err) = sock.set_multicast_ttl_v4(hop_count) {
                        space.assert(t, language(), &udp::Error {
                            detail: AnyValue::new(format!("set_multicast_ttl_v4: {}", err)),
                        });
                    }
                    Ok(())
                })));
        Ok(())
    }));
    let mut buf = [0; 65536];
    loop {
        let (len, source_addr) = sock.recv_from(&mut buf).await?;
        let bs = &buf[..len];
        let p = udp::PacketIn {
            source_addr: source_addr.ip().to_string(),
            source_port: source_addr.port().into(),
            body: bs.to_vec(),
        };
        tracing::trace!(?p, "inbound packet");
        facet.activate(&account, cause.clone(), |t| {
            space.message(t, language(), &p);
            Ok(())
        });
    }
}

#[tokio::main]
async fn main() -> ActorResult {
    syndicate::convenient_logging()?;
    relay::stdio_service(move |t| {
        let local_ds = Cap::new(&t.create(Dataspace::new(Some(AnyValue::symbol("udp")))));
        during!(t, local_ds, language(), $b: udp::Bind::<AnyValue>, |t: &mut Activation| {
            t.spawn_link(Some(language().unparse(&b)), move |t| {
                let trace_collector = t.trace_collector();
                let account = Account::new(Some(AnyValue::symbol("udp_io")), trace_collector.clone());
                let cause = trace_collector.as_ref().map(|_| trace::TurnCause::external("udp_io"));
                let err_cause = trace_collector.as_ref().map(|_| trace::TurnCause::external("udp_error"));
                let facet = t.facet_ref();
                t.linked_task(Some(AnyValue::symbol("udp_io")), async move {
                    match run_udp_socket(&b, account.clone(), cause, facet.clone()).await {
                        Ok(()) => Ok(LinkedTaskTermination::Normal),
                        Err(err) => {
                            let err_val = AnyValue::new(format!("udp_io: {}", err));
                            facet.activate(&account, err_cause, move |t| {
                                b.space.assert(t, language(), &udp::Error { detail: err_val });
                                Ok(())
                            });
                            Ok(LinkedTaskTermination::KeepFacet)
                        }
                    }
                });
                Ok(())
            });
            Ok(())
        });
        Ok(local_ds)
    }).await
}
